﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GolperRobocop : MonoBehaviour {
	Rigidbody2D rb2d ;
	float timeLeft = 0.5f;


	void Start ()
	{
		rb2d = GetComponent<Rigidbody2D> ();
	}
	void Update ()
	{
		rb2d.transform.Translate (new Vector3 (0.5f, 0, 0));
		timeLeft -= Time.deltaTime;
		if (timeLeft < 0) {
			Destroy (this.gameObject);
		}
	}
	void OnTriggerEnter2D(Collider2D miau)
	{
		Destroy (this.gameObject);
	}
}
