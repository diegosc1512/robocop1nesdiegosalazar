﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class enemyBlueController : MonoBehaviour {
	public float maxSpeed = 20f;
	public float speed=20f;
	private Rigidbody2D rb2d;
	public  bool muerto=false;
	public bool piso;
	private Animator anim;
	float timeLeft = 2.5f;
	float timeBajada = 2.5f;

	void Start () {
		rb2d = GetComponent<Rigidbody2D> ();
		anim = GetComponent<Animator>();
	}

	void Update () {
		if (timeBajada>1f) {
			rb2d.transform.Translate (new Vector3 (0.3f, 0, 0));
			transform.localScale = new Vector3 (1f, 1f, 1f);
		}
		if (timeBajada < 0) {
			rb2d.transform.Translate (new Vector3 (0.3f, 0, 0));
			transform.localScale = new Vector3 (1f, 1f, 1f);
		}
		anim.SetBool ("piso", piso);
		anim.SetBool ("muerto", muerto);
		if (muerto==true) {
			timeLeft -= Time.deltaTime;
			if (timeLeft > 0&&timeLeft < 1.20f) {
				rb2d.transform.Translate (new Vector3 (-0.5f, -0.5f, 0));

			}
			if (timeLeft > 1.20f) {
				rb2d.transform.Translate (new Vector3 (-0.5f, 0.2f, 0));

			}
			if (timeLeft < 0) {
				Destroy (this.gameObject);
			}
		}
	}
	void OnTriggerEnter2D(Collider2D miau)
	{
		piso = true;

		if (miau.gameObject.name=="BalaDisparo_0(Clone)"||miau.gameObject.name=="BalaDisparoIzquierda(Clone)") {

			piso = false;
			muerto = true;
			//añair el puntaje
		}
	}
 
}
