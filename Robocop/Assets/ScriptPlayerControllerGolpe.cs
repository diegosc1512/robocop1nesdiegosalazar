﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class ScriptPlayerControllerGolpe : MonoBehaviour {
	public float vida=100;
	public float speed= 5f;
	public bool movimiento=false;
	public float maxSpeed=2f;
	public bool grounded;
	public bool ladoDerecho=true;
	public bool ePush=false;
	public bool wPush = false;
	public static bool sPush = false;
	Time tiempo;
	public bool disparo=false;
	float timeLeft = 100f;

	private Transform posPistola;
	private Transform posPistolaArriba;
	private Transform posPistolaIzquierda;
	private Transform posPistolaDiagonal;
	private Transform posPistolaDiagonalIzquierda;
	public GameObject bala;
	public GameObject balaArriba;
	public GameObject balaIzquierda;
	public GameObject balaDiagonal;
	public GameObject balaDiagonalIzquierda;
	private Rigidbody2D rb2d;
	private Animator anim;

	void Start () {
		rb2d = GetComponent<Rigidbody2D> ();
		anim = GetComponent<Animator>();
		posPistola = GameObject.Find ("Pistola").transform;
		posPistolaArriba = GameObject.Find ("PistolaArriba").transform;
		posPistolaIzquierda = GameObject.Find ("PistolaIzquierda").transform;
		posPistolaDiagonal = GameObject.Find ("PistolaDiagonal").transform;
		posPistolaDiagonalIzquierda = GameObject.Find ("PistolaIzquierdaDiagonal").transform;
	}

	void Update () {
		anim.SetFloat ("Speed",Mathf.Abs(rb2d.velocity.x));
		anim.SetBool ("Grounded", grounded);
		anim.SetBool ("Movimiento", movimiento);
		anim.SetBool ("disparo", disparo);
		anim.SetBool ("ePush", ePush);
		anim.SetBool ("wPush", wPush);
		anim.SetBool ("sPush", sPush);

		//si no jala borrar

		if (Input.GetKey (KeyCode.D) && !Input.GetKey(KeyCode.W)&& !Input.GetKey(KeyCode.E)) {
			rb2d.transform.Translate (new Vector3 (0.2f, 0, 0));
			transform.localScale = new Vector3 (1f, 1f, 1f);
			movimiento = true;
			ladoDerecho = true;
			anim.SetBool ("Movimiento", movimiento);
			Mover ();

		}


		else {
			movimiento = false;
		}

		if (Input.GetKeyDown (KeyCode.K)&& !Input.GetKey(KeyCode.W)&& !Input.GetKey(KeyCode.E)&& !Input.GetKey(KeyCode.Q)&&!Input.GetKey(KeyCode.S)) 
		{
			if (ladoDerecho==false)
			{
				Instantiate (balaIzquierda, posPistolaIzquierda.position, new Quaternion ());	

			}
			if (ladoDerecho==true)
			{
				Instantiate (bala, posPistola.position, new Quaternion ());	
			} 
			disparo = true;
			Controladores ();
		}
		if (!Input.GetKeyDown (KeyCode.K)) {
			disparo = false;
		}

		//if (Input.GetKeyDown(KeyCode.K)&&Input.GetKey(KeyCode.W)&&Input.GetKey(KeyCode.D)) 
		if (Input.GetKey(KeyCode.E)&&Input.GetKeyDown(KeyCode.K)&&ladoDerecho) 
		{
			Instantiate (balaDiagonal, posPistolaDiagonal.position, new Quaternion ());	
			disparo = false;
			ePush = true;
			Controladores ();

		}
		if (!Input.GetKey (KeyCode.E)) {
			ePush = false;
		}

		if (Input.GetKey(KeyCode.Q)&&Input.GetKeyDown(KeyCode.K)&&ladoDerecho==false) 
		{
			Instantiate (balaDiagonalIzquierda, posPistolaDiagonalIzquierda.position, new Quaternion ());
			disparo = false;
			ePush = true;
			Controladores ();
		}
		if (!Input.GetKey (KeyCode.Q)) {
			ePush = false;
		}
		if (Input.GetKey(KeyCode.W)&&Input.GetKeyDown(KeyCode.K)) 
		{
			Instantiate (balaArriba, posPistolaArriba.position, new Quaternion ());	
			disparo = false;
			wPush = true;
			Controladores ();
		}
		if (!Input.GetKey(KeyCode.W)) {
			wPush = false;
		}

		if (Input.GetKey(KeyCode.S)&&!Input.GetKeyDown(KeyCode.K)) 
		{
			disparo = false;
			sPush = true;
			Controladores ();
		}
		if (!Input.GetKey(KeyCode.S)||Input.GetKey(KeyCode.D)||Input.GetKey(KeyCode.W)||Input.GetKeyDown(KeyCode.K)) {
			sPush = false;
		}
		if (Input.GetKey(KeyCode.M)) {
			ChangeScene ();
		}

	}
	#region metodos
	void Controladores()
	{
		anim.SetBool ("Movimiento", movimiento);
		anim.SetBool ("disparo", disparo);
		anim.SetBool ("ePush", ePush);
		anim.SetBool ("wPush", wPush);
		anim.SetBool ("sPush", sPush);

	}
	void Mover()
	{
		wPush = false;
		ePush = false;
		sPush = false;
	}
	//void Awake()
	//{
	//	anim = GetComponent <Animator> ();
	//Application.LoadLevel (Application.loadedLevel);
	//}
	void OnTriggerEnter2D(Collider2D miau)
	{
		timeLeft = timeLeft - 1f;
		if (miau.gameObject.name=="boss1") {
		//if (timeLeft%2==0) {

			vida=vida-5;
			if (vida<=0) {
				//anim.SetTrigger ("GameOver");
				//Destroy (this.gameObject);
				SceneManager.LoadScene ("RobocopLvl1.1");
			}

		}
	}
	public void ChangeScene()
	{
		SceneManager.LoadScene ("Lvl2Robocop");

	}
	#endregion
}
