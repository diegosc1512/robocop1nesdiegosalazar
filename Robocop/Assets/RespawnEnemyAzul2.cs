﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RespawnEnemyAzul2 : MonoBehaviour {
	private Transform posRespawn;
	public GameObject enemigo;
	public float contador = 0;

	void Start () {
		posRespawn = GameObject.Find ("RespawnAzul2").transform;
	}

	void Update () {

	}

	void OnTriggerEnter2D(Collider2D miau)
	{
		contador++;
		if (miau.gameObject.name=="BarreraI"&&contador<7)
		{
			Instantiate (enemigo, posRespawn.position, new Quaternion ());
			//añair el puntaje
		}
	}
}
