﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BloqueDfolllow : MonoBehaviour {
	public GameObject follow;
	public Vector2 minCameraPos, maxCameraPos;
	void Start () {

	}
	void FixedUpdate () {
		float posX = follow.transform.position.x+12f;
		float posY = follow.transform.position.y;
		transform.position = new Vector3 (
			Mathf.Clamp (posX, minCameraPos.x, maxCameraPos.x),
			Mathf.Clamp (posY, minCameraPos.y, maxCameraPos.y),
			transform.position.z);
	}
}
